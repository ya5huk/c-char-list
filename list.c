#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct list
{
	char** elements;
	int numElements;
} list;

void addElement(list* lst, char element[]);
void printList(list lst);
void freeListMemory(list lst);

int main(void)
{
    // Feel free to use it as you like!
    // Don't forget to free the dynamic memory once you are done.
    // You can use the function --> freeListMemory
    return 0;
}

void addElement(list* lst, char element[])
{
	/*
	Function takes a list object and adds an element to it
	Input: the object and the element
	Output: None
	*/
	lst->elements[lst->numElements] = (char*)malloc(sizeof(char) * strlen(element)); // we go to the last index in elements, get new memory to the currect index and fill it with out new word
	strcpy(lst->elements[lst->numElements], element);
	lst->numElements++;
	
}

void printList(list lst)
{
	/*
	Function prints all elements of the list
	Input: list object
	Output: None
	*/
	int i = 0;
	for (i = 0; i < lst.numElements; i++)
	{
		printf("%s\n", lst.elements[i]);
	}
}

void freeListMemory(list lst)
{
	/*
	Funtion frees all the memory of the list object
	Input: list object
	Output: None
	*/
	int i = 0;
	for (i = 0; i < lst.numElements; i++)
	{
		free(lst.elements[i]);
	}
	free(lst.elements);
}
